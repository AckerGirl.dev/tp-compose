flask
redis

# deploiement sur aws ou sur VPS(à chercher comment le faire)
1- se connecter à la console
2- creer une machine EC2 ubuntu
3- se connecter à la machine avec une clé ssh
4- installer docker sur la machine EC2 (voir la doc docker)
5- installer docker-compose sur la machine EC2 (voir doc docker)
6- mettre à jour la machine (apt-get update)
7- cloner le code source à partir de gitlab
8- lancer l'application avec docker-compose (docker-compose up -d)

#automatisation de la creation de nos images basées sur l'application (gitlab ci)
1- creer le fichier .gitlab-ci.yml
2- utiliser le gitlab-registry
3- checker la doc de gitlab pour voir les variables d'environnement predefinies pour le ci/cd
    pour les utiliser dans notre fichier .gitlab-ci.yml
4- faire le commit apres la creation du fichier .gitlab-ci.yml
5- aller sur gitlab et voir la section CI/CD -> piplines -> 
6- générer un token sur gitlab pour s'authentifier au gitlab_registry afin de récupérer une image(settings->Repository->Deploy tokens->Expand)
    - renseigner un nom pour le token (cicd)
    - cocher la case (read_registry)

#automatisation du déploiement (cd) -> elle peut se faire de plusieurs façons dont:
1- créer une paire de clé ssh
    - ssh-keygen -b taille_clé(4096)
2- récupérer la clé publique
3- copier la clé privé
    - créer une variable d'environnement (PKEY)
    - coller la clé privé dans le champs "value"
    - selectionner l'option "file" pour une variable de type fichier
4- creer une variable SERVER_IP = @ip_serveur (machine EC2)
5- creer une variable SERVER_USERNAME = "ubuntu" (nom de l'utilisateur connecté sur la machine EC2 ici ubuntu)
6- creer une variable d'environnement PROJECT = "/home/ubuntu" (dossier du projet)
7- ajouter l'utilisateur ubuntu à docker